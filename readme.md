# <PROJECT NAME> (<PROJECT START DATE>)

## Tech Stack

- Laravel v7 and Blade
- SASS

## Development

### Requirements

- Docker
- Git

### Setup

1. Open Terminal.
2. `cd` into your workspace directory.
3. Clone the repo using Git - [<GIT REPO URL>](<GIT REPO URL>). Use `<PROJECT NAME>` as the project name.
4. `cd` into the project root.
5. Create a `.env` file using the the relevant template.
6. Run `docker-compose up`.
7. Open a new Terminal tab and `cd` into the project root.
8. Run `docker exec -it <PROJECT NAME>_php_1 /bin/bash`.
9. Run `composer install`.
10. Open a new Terminal tab and `cd` into the project root.
11. Run `docker exec -it <PROJECT NAME>_nodejs_1 /bin/bash`.
12. Run `yarn install`.

## Production

### Requirements

- Apache v2.4 or higher **OR** Nginx v16 or higher
- PHP v7.2 or higher
  - BCMath PHP Extension
  - Ctype PHP Extension
  - JSON PHP Extension
  - Mbstring PHP Extension
  - OpenSSL PHP Extension
  - PDO PHP Extension
  - Tokenizer PHP Extension
  - XML PHP Extension
- MySQL v5.7 or higher **OR** MariaDB v10.2 or higher
- Git
- Composer
- Node.js LTS
- NPM **OR** Yarn

### Setup

1. Clone the repo using Git - [<GIT REPO URL>](<GIT REPO URL>). Use `<DOMAIN>` as the project name.
2. Create a new MySQL database using **utf8mb4** called `<PROJECT NAME>`.
3. `cd` into the root of the project.
4. Create a new `.env` file using the relevant environment template.
5. Run `composer install`.
6. Run `yarn install`.
7. Run `php artisan migrate`.
8. Run `yarn run lm-prod`.
